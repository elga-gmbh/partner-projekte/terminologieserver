> Das vollständige Projekt des Terminologie-Servers, welcher von 2013 bis 2022 unter https://termpub.gesundheit.gv.at/ bereit stand. Weiterentwickelt vom Technikum Wien. Code unter https://github.com/TechnikumWienAcademy/Terminologieserver auffindbar.

> Vollständig ersetzt durch https://gitlab.com/elga-gmbh/termgit-dev.
